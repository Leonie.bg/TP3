// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var bcrypt = require('bcrypt');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.post("/login", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users WHERE ident = ? AND password = ?;', [req.body.login, req.body.password], function(errorLogin, dataLogin) {
		if(errorLogin) {
			res.json({status:false});
		} else {
			if(dataLogin.length > 0)	{
				db.all('SELECT ident, token FROM sessions WHERE ident = ?', dataLogin[0].ident, function(errorToken, dataToken) {
					if(errorToken) {
						res.json({status:false});						
					} else {
						TokenToHash = dataLogin[0].ident + Date.now();
						bcrypt.hash(TokenToHash, 10, function(errorHash, hash) {
							if(errorHash) {
								res.json({status:false});
							} else {
								if(dataToken[0].token.length < 0) {
									db.all('INSERT INTO sessions (ident, token) VALUES (?, ?)', [dataLogin[0].ident, hash], function(error, data) {
										if(error) {
											res.json({status:false});
										} else {
											res.cookie('token', hash);
											res.json({status:true, tokenUpdate: hash});
										}
									});
								} else {
									db.all('UPDATE sessions SET token = ? WHERE ident = ?', [hash, dataLogin[0].ident], function(error, data) {
										if(error) {
											res.json({status:false});
										} else {
											res.cookie('token', hash);
											res.json({status:true, tokenUpdate: hash});
											
										}	
									});
								}
							}
						});
					}
				});
			}
		}
	});
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
